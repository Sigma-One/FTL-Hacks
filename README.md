# Various FTL patches

### About

This repository houses various small patch mods for the game FTL: Faster Than Light.

All patches are made using xdelta3 for the 64-bit Linux version of the game. I have no intention of making ones for 32-bit Linux or any Windows or MacOS version, as I do not have systems running those OSes or the interest to do so.

### Installation

Apply the patches to FTL.amd64 using xdelta3: `xdelta3 -d -s <FTL_EXEC> <PATCH_FILE> <PATCHED_EXEC_NAME>`. FTL.amd64 can be found in the FTL installation directory under the `data` directory, for example on my system with the Steam version of FTL: `/home/sigma1/.local/share/Steam/steamapps/common/FTL Faster Than Light/data/FTL.amd64`
